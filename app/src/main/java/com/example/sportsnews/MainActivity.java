package com.example.sportsnews;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {
    //אלאמנט המכיל את תוצאות המשחקים
    private RecyclerView mScoreRecyclerView;
    //אובייקט שאחראי על התאמת המידע למשחקים ע"פ מיקום
    private RecyclerView.Adapter mScoreAdapter;
    //אובייקט שמתאר איך המידע יוצג (בשורות אחד אחרי השני)
    private LinearLayoutManager mLayoutManager;
    //מייצג את השורה המפריד בין האלמנטים בrecyclerview
    private DividerItemDecoration mDivider;

    private HashMap<Integer, JSONObject> mTeamData;
    private ArrayList<GameSummary> mGameSummaryArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpProperties();
        setUpTeamDataRequest();
//        setUpPremierLeagueSeasonRequest();
        setUpScoreRecyclerView();
    }

    //פעולה שמגדירה הפניות לתכונות המחלקה
    private void setUpProperties() {
        mTeamData = new HashMap<>();
        mGameSummaryArr = new ArrayList<>();
        mScoreRecyclerView = findViewById(R.id.score_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mScoreAdapter = new GameSummaryAdapter(mGameSummaryArr);
        mDivider = new DividerItemDecoration(
                mScoreRecyclerView.getContext(),
                mLayoutManager.getOrientation());
    }

    private void setUpTeamDataRequest() {
        ApiFootballRequest request = new ApiFootballRequest(Method.GET, ApiFootballRequest.Uri.CURRENT_SEASON_TEAMS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray teamArr = response.getJSONArray("data");
                    for (int i = 0; i < teamArr.length(); i++) {
                        JSONObject team = teamArr.getJSONObject(i);
                        mTeamData.put(team.getInt("id"), team);
                    }
                    setUpPremierLeagueSeasonRequest();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorResponse", error.getMessage());
            }
        });
        ReqQueue.getInstance(this).add(request);
    }

    private void setUpPremierLeagueSeasonRequest() {

        Log.i("p_url", ApiFootballRequest.Uri.PREMIER_LEAGUE_SEASON);
        ApiFootballRequest request = new ApiFootballRequest(Method.GET, ApiFootballRequest.Uri.PREMIER_LEAGUE_SEASON, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject results = data.getJSONObject("results");
                    JSONArray resultsData = results.getJSONArray("data");
                    for (int i = 0; i < resultsData.length(); i++) {
                        JSONObject object = resultsData.getJSONObject(i);
                        int homeTeamId = object.getInt("localteam_id");
                        int awayTeamId = object.getInt("visitorteam_id");

                        mGameSummaryArr.add(new GameSummary(
                                object,
                                mTeamData.get(homeTeamId),
                                mTeamData.get(awayTeamId))
                        );
                    }

                    mScoreAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorResponse", error.getMessage());
            }
        });
        ReqQueue.getInstance(this).add(request);
    }

    //פעולה שמפעילה את הrecyclerview
    private void setUpScoreRecyclerView() {
        mScoreRecyclerView.setLayoutManager(mLayoutManager);
        mScoreRecyclerView.addItemDecoration(mDivider);
        mScoreRecyclerView.setAdapter(mScoreAdapter);
    }


}
