package com.example.sportsnews;

import android.support.annotation.Nullable;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

public class ApiFootballRequest extends JsonObjectRequest {
    public static class Uri {
        public static final String BASE_URL = "https://soccer.sportmonks.com/api/v2.0";
        public static final String TOKEN = "DHSsCdGJMLPNkjLPSjS3GvVpHxhRqbVx8uTcmvxmIUON12llRQKqxyiHE8CI";

        public static final String PREMIER_LEAGUE_SEASON = BASE_URL + "/seasons/12963?api_token=" + TOKEN + "&include=results";
        public static final String CURRENT_SEASON_TEAMS = BASE_URL + "/teams/season/12963?api_token=" + TOKEN;
        public static final String GAME_EVENT = BASE_URL + "/fixtures/%s?api_token=" + TOKEN +"&include=events";

    }


    public ApiFootballRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }
}
