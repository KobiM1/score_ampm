package com.example.sportsnews;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

//המחלקה פותחת thread בשביל להוריד את התמונות של הקבוצות מהשרת
public class DownloadImg extends AsyncTask<String, Void, Bitmap> {
    private ImageView imageView;

    public DownloadImg(ImageView imageView) {
        this.imageView = imageView;
    }

    //כשאובייקט מהמחלקה נוצר הפעולה הזאת נקראת האופן אוטומטי
    //הפעולה מקבלת מערך אין סופי של string.
    //כל סטרינג הוא קישור לאייקון של הקבוצה בשרת
    //הפעולה מורידה את האייקון מהקישור
    @Override
    protected Bitmap doInBackground(String... strings) {
        String src = strings[0];
        try {
            InputStream stream = new URL(src).openStream();
            return BitmapFactory.decodeStream(stream);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //פעולה שנקראת אחרי שההורדה מסתיימת
    //הפעולה שמה את התמונה שירדה עבור הקבוצה המתאימה
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        this.imageView.setImageBitmap(bitmap);
    }
}
