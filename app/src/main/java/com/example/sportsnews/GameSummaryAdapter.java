package com.example.sportsnews;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class GameSummaryAdapter extends RecyclerView.Adapter<GameSummaryAdapter.ScoreViewHolder> {
    public final String GAME_ID = "gameId";
    public ArrayList<GameSummary> gameSummaryArr;


    public GameSummaryAdapter(ArrayList<GameSummary> gameEventArr) {
        this.gameSummaryArr = gameEventArr;
    }

    @NonNull
    @Override
    public ScoreViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        View v = (View) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.game_summary_view, viewGroup, false);

        ScoreViewHolder vh = new ScoreViewHolder(v);
        return vh;
    }

    @Override
    public int getItemCount() {
        return gameSummaryArr.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ScoreViewHolder scoreViewHolder, final int i) {
        final GameSummary gameSummary = gameSummaryArr.get(i);

        scoreViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                //אובייקט המקשר את המעבר בין המסך הנוכחי למסך הבא
                Intent intent = new Intent(context, GameEventActivity.class);
                //שמירת הID של המשחק הנוכחי כפרמטר למסך הבא
                intent.putExtra("gameSummary", gameSummary);

                intent.putExtra(GAME_ID, gameSummary.gameId);
                intent.putExtra("homeTeamId", gameSummary.homeTeamId);
                intent.putExtra("awayTeamId", gameSummary.awayTeamId);
                //פתיחת מסך עם מידע על המשחק
                context.startActivity(intent);
            }
        });

        scoreViewHolder.homeTeam.setText(gameSummary.homeTeamName);
        scoreViewHolder.awayTeam.setText(gameSummary.awayTeamName);
        scoreViewHolder.score.setText(gameSummary.score);
    }

    public static class ScoreViewHolder extends RecyclerView.ViewHolder {

        public TextView homeTeam;
        public ImageView homeIcon;
        public TextView score;
        public TextView awayTeam;
        public ImageView awayIcon;


        public ScoreViewHolder(View v) {
            super(v);

            homeTeam = v.findViewById(R.id.home_team_name);
            homeIcon = v.findViewById(R.id.home_team_icon);
            score = v.findViewById(R.id.score);
            awayTeam = v.findViewById(R.id.away_team_name);
            awayIcon = v.findViewById(R.id.away_team_icon);
        }
    }
}
