package com.example.sportsnews;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class ReqQueue {
    private static ReqQueue instance;
    private RequestQueue mRequestQueue;
    private static Context mContext;

    private ReqQueue(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized ReqQueue getInstance(Context context) {
        if (instance == null) {
            instance = new ReqQueue(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }


    public <T> void add(Request<T> req) {
        getRequestQueue().add(req);
    }
}