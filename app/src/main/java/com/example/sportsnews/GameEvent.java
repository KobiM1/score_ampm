package com.example.sportsnews;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class GameEvent {
    public int id;
    public int teamId;
    public String type;
    public String playerName;
    public String relatedPlayerName;
    public int minute;

    public GameEvent(JSONObject object) {
        try {
            id = object.getInt("id");
            teamId = object.getInt("team_id");
            type = object.getString("type");
            playerName = object.getString("player_name");
            relatedPlayerName = object.getString("related_player_name");
            minute = object.getInt("minute");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
