package com.example.sportsnews;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class GameEventActivity extends AppCompatActivity {
    private RecyclerView mInfoRecyclerView;
    //אובייקט שאחראי על התאמת המידע למשחקים ע"פ מיקום
    private RecyclerView.Adapter mInfoAdapter;
    //אובייקט שמתאר איך המידע יוצג (בשורות אחד אחרי השני)
    private LinearLayoutManager mLayoutManager;
    //מייצג את השורה המפריד בין האלמנטים בrecyclerview
    private DividerItemDecoration mDivider;
    //מערך המתארך את רצץ האירועים במשחק
    private ArrayList<GameEvent> gameEventArr;
    //אובייקט שמכיל את המידע על סיכום המשחק
    private GameSummary gameSummary;
    //תמונת קבוצת הבית
    private ImageView homeTeamIcon;
    //תמונת קבוצת החוץ
    private ImageView awayTeamIcon;
    //תוצאת משחק
    private TextView score;
    //תאריך קיום המשחק
    private TextView date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_event);

        setUpProperties();
        new DownloadImg(homeTeamIcon).execute(gameSummary.homeTeamIcon);
        new DownloadImg(awayTeamIcon).execute(gameSummary.awayTeamIcon);
        date.setText(gameSummary.date);
        score.setText(gameSummary.score);
        setUpScoreRecyclerView();
        setUpGameEventsRequest();
    }

    //פעולה שמגדירה הפניות לתכונות המחלקה
    private void setUpProperties() {
        gameSummary = (GameSummary) getIntent().getSerializableExtra("gameSummary");
        gameEventArr = new ArrayList<>();
        mInfoRecyclerView = findViewById(R.id.events_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mInfoAdapter = new GameEventAdapter(
                gameEventArr,
                gameSummary.awayTeamId,
                gameSummary.homeTeamId
        );
        mDivider = new DividerItemDecoration(
                mInfoRecyclerView.getContext(),
                mLayoutManager.getOrientation());

        homeTeamIcon = findViewById(R.id.home_team_img);
        awayTeamIcon = findViewById(R.id.away_team_img);
        date = findViewById(R.id.game_date);
        score = findViewById(R.id.game_score);
    }


    //פעולה שמפעילה את הrecyclerview
    private void setUpScoreRecyclerView() {
        mInfoRecyclerView.setLayoutManager(mLayoutManager);
        mInfoRecyclerView.addItemDecoration(mDivider);
        mInfoRecyclerView.setAdapter(mInfoAdapter);

    }


    private void setUpGameEventsRequest() {
        String gameEventUrl = String.format(ApiFootballRequest.Uri.GAME_EVENT, gameSummary.gameId);

        ApiFootballRequest request = new ApiFootballRequest(Request.Method.GET, gameEventUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject events = data.getJSONObject("events");
                    JSONArray eventsData = events.getJSONArray("data");
                    for (int i = 0; i < eventsData.length(); i++) {
                        JSONObject e = eventsData.getJSONObject(i);
                        gameEventArr.add(new GameEvent(e));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mInfoAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorResponse", error.toString());
            }
        });
        ReqQueue.getInstance(this).add(request);
    }


}
