package com.example.sportsnews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class GameEventAdapter extends RecyclerView.Adapter<GameEventAdapter.InfoViewHolder> {

    private int homeTeamId;
    private int awayTeamId;

    public ArrayList<GameEvent> gameEventArr;


    public GameEventAdapter(ArrayList<GameEvent> gameEventArr, int awayTeamId, int homeTeamId) {
        this.gameEventArr = gameEventArr;
        this.homeTeamId = homeTeamId;
        this.awayTeamId = awayTeamId;
    }

    @NonNull
    @Override
    public GameEventAdapter.InfoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = (View) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.game_event_view, viewGroup, false);
        GameEventAdapter.InfoViewHolder vh = new GameEventAdapter.InfoViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull GameEventAdapter.InfoViewHolder infoViewHolder, int i) {
        GameEvent event = gameEventArr.get(i);
        if (event.teamId == this.homeTeamId) {
            infoViewHolder.homeEvent.setText(event.playerName);
            infoViewHolder.homeEventType.setText(event.type);

        } else if (event.teamId == this.awayTeamId){
            infoViewHolder.awayEvent.setText(event.playerName);
            infoViewHolder.awayEventType.setText(event.type);
        }
        infoViewHolder.minute.setText(String.valueOf(event.minute));
    }

    @Override
    public int getItemCount() {
        return gameEventArr.size();
    }

    public static class InfoViewHolder extends RecyclerView.ViewHolder {
        public TextView homeEvent;
        public TextView homeEventType;
        public TextView minute;
        public TextView awayEventType;
        public TextView awayEvent;

        public InfoViewHolder(View v) {
            super(v);
            homeEvent = v.findViewById(R.id.home_event);
            homeEventType = v.findViewById(R.id.home_event_type);
            minute = v.findViewById(R.id.minute);
            awayEventType = v.findViewById(R.id.away_event_type);
            awayEvent = v.findViewById(R.id.away_event);
        }
    }
}
