package com.example.sportsnews;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class GameSummary implements Serializable {
    public int gameId;
    public int homeTeamId;
    public String homeTeamName;
    public String homeTeamIcon;

    public int awayTeamId;
    public String awayTeamName;
    public String awayTeamIcon;
    public String date;
    public String score;

    public GameSummary(JSONObject object, JSONObject homeTeam, JSONObject awayTeam) {
        try {
            this.gameId = object.getInt("id");
            this.score = object.getJSONObject("scores").getString("ft_score");
            this.date = object.getJSONObject("time").getJSONObject("starting_at").getString("date");
            this.homeTeamId = homeTeam.getInt("id");
            this.homeTeamName = homeTeam.getString("name");
            this.homeTeamIcon = homeTeam.getString("logo_path");

            this.awayTeamId = awayTeam.getInt("id");
            this.awayTeamName = awayTeam.getString("name");
            this.awayTeamIcon = awayTeam.getString("logo_path");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}